Report: 

Date: 24.4.21
Present at the meeting: Tom Arne, Sang, Mette, Yan, Ali

Agenda: Presentation of the program, feedback from Ali

Feedback: 

- Program: 
  - Sort by time/priority 
  - Add priority
  - Double zero bug

- User manual: 
  - Detailed description
  - Step-by-step description 
  - Test the user manual on someone else than in the team 

- Documentation in general:
  - Hand in a pdf that contain all the documentation and diagrams from the whole project
  - Table of contents 