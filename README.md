# PROG1004_2021_group16

## Collaboration agreement for Team 16

Members: Tom Arne Haugen Brandvold, Sang Ngoc Nguyen, Anders Lirhus and Mette Derås Waatsveen 

# Introduction

The Collaboration agreement is based on a collection of goals, role responsibilities, procedures, and guidelines for interaction within the team. The agreement has elements that are considered important to achieve the goals of this project. The agreement is followed until the end of the project and is binding.

# Goals 

### Effect goals
1. Working environment

First, to achieve a good and effective workflow, the working environment is important. By that we wish to build trust in each other so that we can do each other good.  

2. Time disposal

It is encouraged to ask for help within the team or the TA if a team member have questions about the assignment to avoid getting stuck for too long with one task. It is expected that there is a clear agenda to each meeting. It is expected that everyone knows what to do to until the next meeting. If anything is unclear, they should ask. 

3. Roles

We assume that there will be varying roles from week to week. This to gain experience with the different roles. At the beginning of each meeting there will be a summary of how it goes with every simple interference work. At the end of the meeting new assignment should be distributed. 

### Result goals
1. Teamwork

We should learn to work in a team by reflecting over the process and learn by it. That consider the teamwork and the professional quality of the product. 

2. Deadlines

To keep the deadlines should the team implement and try to do good planning, help each other, and support each other. 

# Role responsibilities

To organize the teamwork will we establish different roles. We will distribute the different roles between ourselves in a rotation like manner. 

* Project leader

The project manager's overall task is to delegate tasks, as well as keep the agenda at the meetings.

* Quality assurance

The quality assurance's overall task is to see through the assignments before deadline to assure everything is included, ask questions, and discuss any changes. However, it is expected that everyone aims to get everything in their respective task finished to deadline. 

* Organization of meetings/documentation

Responsible for convening meetings and organizing meeting locations, responsible for writing the reference sheet and documenting work process. 


# Procedures for teamwork

### A. Meetings
We will have a weekly meeting with our Learning Assistant (LA). Time and place for these meetings will be decided together with the LA and all group members.

### B. Notification in case of absence or other incidents 
When a member is not able to attend a planned meeting he/she should notify the project leader or the responsible of the meeting at least one day prior to the meeting.
If the group member is not able to give a notice before the meeting, an explenation of the reason for not meeting should be provided to the group leader as soon as possible.

### C. Submission of team work
The group should agree on who is resposible for making shure all the groups work and tasks are being submittet within the deadline. Before submitting, the resposible member should make shure all of the members agree that the work is ready for submitting. In the case of an disagreement the group leader should help the group come to an agreement on what to do. If this cant be done, the teacher must be notified.

# Interaction 
### A. Attendance and preparation
When a meeting is planned, all members should attend and come prepeared, unless other is agreed upon.
### B. Presence and commitment
All members commit to contribute to reach the goals of the group for the project. If a member does not contribute in a manner that is satisfying to the rest of the group this first need to be discussed with the member to find a solutoin to this problem. If this does not solve the problem the teacher must be notified. 
### C. How to support each other
In order to have a good working environment it is important that the members make shure to compliment each others work. They should also make shure to offer helping out when a member have difficulties. It is important that the members let the others know when he/she needs help or do not understand the task that have been given. 
### D. Disagreement, breach of contract
In the case of a disagreement or breach of the contract, this should first be discussed within the group to try to find a solution that is satisfying to the whole group. If a solution id not found, the issue must be discussed with the teacher. 

# Signatures


