/**
 * Todo-program specified towards university lecturers
 *
 * PROG1004 – Programmvareutvikling, spring 2021
 *
 * @file todoapp.cpp
 * @author Tom Arne Brandvold, Sang N. Nguyen
 */


#include <iostream>              //  cout, cin
#include <fstream>               //  ifstream, ofstream
#include <string>
#include <vector>
#include <map>
#include "LesData2.h"            //  For reading div data (By Frode Haug)
using namespace std;


enum TaskType {M, P, C};
//vector <Task*> gTasks;


/**
 *  Task
 */
class Task {
    private:
        int day, month, year;
        bool finished;
        char priority;
        TaskType tType;

    public:
        string description;
        //char taskType;

        Task(TaskType t)  {day = month = year = 0; description = ""; finished = false; tType = t;}
        //Task(char type)  {day = month = year = 0; description = ""; finished = false; this->taskType = type;}
        Task(ifstream & inn, TaskType t);
        virtual void changeData();
        virtual void readData();
        virtual void writeData() const;
        virtual void writeToFile(ofstream & ut) const;
        void finishTask() {finished = true;}
        bool typeMatch(TaskType mT) {return (mT == tType);}
        bool priorityMatch(char p) {return (p == priority);}
        virtual string dateString() = 0;
};


/**
 *  Meeting
 */
class Meeting : public Task {
    private:
        int startHour, startMin;
        int endHour, endMin;
        string place;
    public:
          Meeting() : Task(M)  {startHour = startMin = endHour = endMin = 0; place = "";}
          //Meeting(char type)  {startHour = startMin = endHour = endMin = 0; place = "", taskType = 'M';}
          Meeting(ifstream & inn);
          virtual void changeData();
          virtual void readData();
          virtual void writeData() const;
          virtual void writeToFile(ofstream & ut) const;
          virtual string dateString();
};


/**
 *  Preperation (for class)
 */
class Preperation : public Task {
    private:
        int hour, minute;       //  deadline
        string subjectCode;
    public:
          Preperation() : Task(P)   {hour = minute = 0; subjectCode = "";}
          //Preperation(char type)  {hour = minute = 0; subjectCode = "", taskType = 'P';}
          Preperation(ifstream & inn);
          virtual void changeData();
          virtual void readData();
          virtual void writeData() const;
          virtual void writeToFile(ofstream & ut) const;
          virtual string dateString();
};


/**
 *  Correction (of test/assignment)
 */
class Correction : public Task {
    private:
        int hour, minute;       //  deadline
        int amount;             //  of tests/assignments
        string subjectCode;

    public:
          Correction() : Task(C)  {hour = minute = amount = 0; subjectCode = "";}
          //Correction(char type)  {hour = minute = amount = 0; subjectCode = "", taskType = 'C';}
          Correction(ifstream & inn);
          virtual void changeData();
          virtual void readData();
          virtual void writeData() const;
          virtual void writeToFile(ofstream & ut) const;
          virtual string dateString();
};


void changeOneTask();
void readFromFile();
void newTask();
void writeAllTasks();
void writeMeny();
void writeToFile();
void finishOneTask();
void filterByType();
void deleteTask();
void sortByDate();
void sortByPriority();

vector <Task*> gTasks;
map <string, Task*> gSortedTasks;


/**
 *  Main program
 */
int main(void)  {
    char kommando;

    readFromFile();
    writeMeny();
    kommando = lesChar("Command");

    while (kommando != 'Q') {
        switch (kommando) {
            case 'N':   newTask();              break;
            case 'A':   writeAllTasks();        break;
            case 'T':   filterByType();         break;
            case 'E':   changeOneTask();        break;
            case 'F':   finishOneTask();        break;
            case 'D':   deleteTask();           break;
            default:    writeMeny();            break;
        }
        writeMeny();
        kommando = lesChar("\nCommand");
    }
    writeToFile();
    gTasks.clear();
    return 0;
}

// ---------------------------------------------------------------------------
//                       CLASS-FUNCTIONS:
// ---------------------------------------------------------------------------

/**
 *  Reads from file
 *
 *  @param   inn  - Fileobject with data
 */
Task::Task(ifstream & inn, TaskType t) {
    tType = t;
    inn.ignore();
    getline(inn, description);
    //inn >> taskType;
    inn >> day >> month >> year >> finished >> priority;
}

void Task::changeData() {
    cout << "\nDescription: ";
    getline(cin, description);
    cout << "Date: " << '\n';
    day = lesInt("Day (DD)", 1, 31);
    month = lesInt("Month (DD)", 1, 12);
    year = lesInt("Year (YYYY)", 2021, 2100);
    do {
        priority = lesChar("Priority (H, M, L)");
    } while (priority != 'H' && priority != 'M' && priority != 'L');
}


/**
 *  Takes input from user
 */
void Task::readData() {
    cout << "\nDescription: ";
    getline(cin, description);
    cout << "Date: " << '\n';
    day = lesInt("Day (DD)", 1, 31);
    month = lesInt("Month (DD)", 1, 12);
    year = lesInt("Year (YYYY)", 2021, 2100);
    do {
        priority = lesChar("Priority (H, M, L)");
    } while (priority != 'H' && priority != 'M' && priority != 'L');
}


/**
 *  Writes task data on screen
 */
void Task::writeData() const {
    cout << description << '\n';
    cout << "Date: " << day << "." << month << "." << year << '\n';
    if (finished == 0) {cout << "Status: Unfinished\n";}
    if (finished == 1) {cout << "Status: Finished\n";}
    if (priority == 'H') {cout << "Priority: High\n";}
    if (priority == 'M') {cout << "Priority: Medium\n";}
    if (priority == 'L') {cout << "Priority: Low\n";}
}


/**
 *  Writes data to file
 *
 *  @param   out  - Fileobject to write to
 */
void Task::writeToFile(ofstream & out) const {
    out << description << '\n';
    //out << taskType << ' ' << day << ' ' <<  month << ' ' <<  year << ' ' <<  finished << ' ' << priority << '\n';
    out << day << ' ' <<  month << ' ' <<  year << ' ' <<  finished << ' ' << priority << '\n';

}

string Task::dateString() {
    string s = to_string(year);
    if (month < 10) {s += to_string(0);}
    s += to_string(month);
    if (day < 10) {s += to_string(0);}
    s += to_string(day);

    return s;
}

// ---------------------------------------------------------------------------

/**
 *  Reads from file
 *
 *  @param   inn  - Fileobject to read from
 */
Meeting::Meeting(ifstream & inn) : Task(inn, M) {
    //inn.ignore();
    inn >> startHour >> startMin >> endHour >> endMin;
    inn.ignore();
    getline(inn, place);
}


/**
 *  Lets user change the task data
 *
 *  @see  writeData()
 */
void Meeting::changeData() {
    Task::changeData();
    startHour = lesInt("Start hour (HH)", 0, 23);
    startMin = lesInt("Start minute (MM)", 0, 59);
    endHour = lesInt("End hour (HH)", 0, 23);
    endMin = lesInt("End minute (MM)", 0, 59);
    cout << "Place: ";
    getline(cin, place);
}

/**
 *  Takes input from user
 *
 *  @see   Task::readData()
 */
void Meeting::readData() {
    Task::readData();
    startHour = lesInt("Start hour (HH)", 0, 23);
    startMin = lesInt("Start minute (MM)", 0, 59);
    endHour = lesInt("End hour (HH)", 0, 23);
    endMin = lesInt("End minute (MM)", 0, 59);
    cout << "Place: ";
    getline(cin, place);
}


/**
 *  Writes task data on screen
 *
 *  @see  Task::writeData()
 */
void Meeting::writeData() const {
    Task::writeData();
    cout << "Start-time: " << startHour << ":";
    if (startMin < 10) {cout << "0";}
    cout << startMin << '\n';
    cout << "End-time:   " << endHour << ":";
    if (endMin < 10) {cout << "0";}
    cout << endMin << '\n';
    cout << "Place: " << place << '\n';
}


/**
 *  Writes data to file
 *
 *  @param   out  - Fileobject to write to
 *  @see     Task::skrivTilTil(...)
 */
void Meeting::writeToFile(ofstream & out) const {
    out << "M ";
    Task::writeToFile(out);
    out << startHour << ' ' << startMin << ' ' << endHour << ' ' << endMin << '\n';
    out << place;
}

string Meeting::dateString() {
    string s;

    s = Task::dateString();

    if (startHour < 10) {s += to_string(0);}
    s += to_string(startHour);
    if (startMin < 10) {s += to_string(0);}
    s += to_string(startMin);

    return s;
}

// ---------------------------------------------------------------------------

/**
 *  Reads from file
 *
 *  @param   inn  - Fileobject to read from
 */
Preperation::Preperation(ifstream & inn) : Task(inn, P) {
    inn >> hour >> minute;
    inn.ignore();
    getline(inn, subjectCode);
}


/**
 *  Lets user change the task data
 *
 *  @see  writeData()
 */
void Preperation::changeData() {
    Task::changeData();
    hour = lesInt("Deadline hour (HH)", 0, 23);
    minute = lesInt("Deadline minute (MM)", 0, 59);
    cout << "Subject Code: "; cin >> subjectCode;

}


/**
 *  Takes input from user
 *
 *  @see   Task::readData()
 */
void Preperation::readData() {
    Task::readData();
    hour = lesInt("Deadline hour (HH)", 0, 23);
    minute = lesInt("Deadline minute (MM)", 0, 59);
    cout << "Subject Code: "; cin >> subjectCode;
}


/**
 *  Writes task data on screen
 *
 *  @see  Task::writeData()
 */
void Preperation::writeData() const {
    cout << "Subject-code: " << subjectCode << '\n';
    Task::writeData();
    cout << "Deadline: " << hour << ":";
    if (minute < 10) {cout << "0";}
    cout << minute << '\n';
}


/**
 *  Writes data to file
 *
 *  @param   out  - Fileobject to write to
 *  @see     Task::writeToFile(...)
 */
void Preperation::writeToFile(ofstream & out) const {
    out << "P ";
    Task::writeToFile(out);
    out << hour << ' ' << minute << '\n';
    out << subjectCode;
}

string Preperation::dateString() {
    string s;

    s = Task::dateString();

    if (hour < 10) {s += to_string(0);}
    s += to_string(hour);
    //if (minute < 10) {s += "0";}
    if (minute < 10) {s += to_string(0);}
    s += to_string(minute);

    return s;
}

// ---------------------------------------------------------------------------

/**
 *  Reads from file
 *
 *  @param   inn  - Fileobject to read from
 */
Correction::Correction(ifstream & inn) : Task(inn, C) {
    inn >> hour >> minute >> amount;
    inn.ignore();
    getline(inn, subjectCode);
}


/**
 *  Lets user change the task data
 *
 *  @see  writeData()
 */
void Correction::changeData() {
    Task::changeData();
    hour = lesInt("Deadline hour (HH)", 0, 23);
    minute = lesInt("Deadline minute (MM)", 0, 59);
    cout << "Subject Code: "; cin >> subjectCode;
    amount = lesInt("Number of assignments/tests to correct", 1, 500);
}


/**
 *  Takes input from user
 *
 *  @see   Task::readData()
 */
void Correction::readData() {
    Task::readData();
    hour = lesInt("Deadline hour (HH)", 0, 23);
    minute = lesInt("Deadline minute (MM)", 0, 59);
    cout << "Subject Code: "; getline(cin, subjectCode);
    amount = lesInt("Number of assignments/tests to correct", 1, 500);
}


/**
 *  Writes task data on screen
 *
 *  @see  Task::writeData()
 */
void Correction::writeData() const {
    cout << "Subject-code: " << subjectCode << '\n';
    Task::writeData();
    cout << "Deadline: " << hour << ":";
    if (minute < 10) {cout << "0";}
    cout << minute << '\n';
    cout << "Tests/assignments to correct: " << amount << '\n';
}


/**
 *  Writes data to file
 *
 *  @param   ut  - Fileobject to write to
 *  @see     Task::skrivTilTil(...)
 */
void Correction::writeToFile(ofstream & out) const {
    out << "C ";
    Task::writeToFile(out);
    out << hour << ' ' << minute << ' ' << amount << '\n';
    out << subjectCode;
}

string Correction::dateString() {
    string s;

    s = Task::dateString();

    if (hour < 10) {s += to_string(0);}
    s += to_string(hour);
    if (minute < 10) {s += to_string(0);}
    s += to_string(minute);

    return s;
}


// ---------------------------------------------------------------------------
//                       OTHER FUNCTIONS:
// ---------------------------------------------------------------------------

/**
 *  Lets the user change a task
 *
 *  @see   virtual Task::changeData()
 */
void changeOneTask() {
    int taskNr;

    if(gTasks.empty()) {
        cout << "No tasks have been added, please add a task first!\n";
    }

    else {
        taskNr = lesInt("Task number", 1, gTasks.size());
        gTasks[taskNr-1]->changeData();
    }
}

/**
 *  Reads task from file and save it in 'gTasks'.
 *
 *  @see  Meeting::Meeting(...)
 *  @see  Correction::Correction(...)
 *  @see  Preperation::Preperation(...)
 */
void readFromFile() {
    char type;
    Task* newTask;

    cout << "Reading from file ..." << '\n';
    ifstream innfil("TODO.DTA");

    innfil >> type;

    if (innfil) {                   //  hvis filen finnes
        while (!innfil.eof()) {     //  looper til fil-slutt nås

            switch (type) {
                case 'M': newTask = new Meeting(innfil);        break;
                case 'P': newTask = new Preperation(innfil);    break;
                case 'C': newTask = new Correction(innfil);     break;
            }

            gTasks.push_back(newTask);

            innfil >> type;
        }
        cout << "Reading from file was successful!" << '\n';
    }
    else {cout << "\nFILE NOT FOUND\n";}

    innfil.close();
}


/**
 *  Adds new task to the datastructure.
 *
 *  @see  Meeting::Meeting()
 *  @see  Correction::Correction()
 *  @see  Preperation::Preperation()
 *  @see  virtual Task::readData()
 */
void newTask() {
    char brukerValg;
    Task* newTask;

    cout << "Task: Meeting (M), Preperation (P), Correction (C)\n";
    do {
        brukerValg = lesChar("Type");
    }while (brukerValg != 'M' && brukerValg != 'P' && brukerValg != 'C' );

   switch (brukerValg) {
            case 'M': newTask = new Meeting();      break;
            case 'P': newTask = new Preperation();  break;
            case 'C': newTask = new Correction();   break;
        }
        newTask->readData();
        gTasks.push_back(newTask);
}


/**
 *  Writes all data for all tasks
 *
 *  @see   virtual Task::writeData()
 */
void sortByNumber() {
    if(gTasks.empty()) {
        cout << "No tasks have been added, please add a task first!\n";
    }

    else {
        for (int i = 0; i < gTasks.size(); i++) {
            cout << "\nTask " << i+1 << '\n';
            gTasks[i]->writeData();
        }
    }
}

/**
 *  Writes all data for a specific task type
 *
 *  @see   virtual Task::writeData()
 */
void filterByType() {
    char brukerValg;
    TaskType chosenType;
    bool match;

    if(gTasks.empty()) {
        cout << "No tasks have been added, please add a task first!\n";
    }

    else {
        do{
            brukerValg = lesChar("Type");
        } while (brukerValg != 'M' && brukerValg != 'P' && brukerValg != 'C' );

        switch (brukerValg) {
            case 'M' : chosenType = M;  break;
            case 'P' : chosenType = P;  break;
            case 'C' : chosenType = C;  break;
        }

        for(int i = 0; i < gTasks.size(); i++) {
            match = (gTasks[i]->typeMatch(chosenType));
            if(match) {
                cout << "\nTask " << i+1 << '\n';
                gTasks[i]->writeData();
            }
        }
    }
}


/**
 *  Writes meny with options on screen.
 */
void writeMeny() {
    cout << "\nAvalible commands:\n"
         << "\tN - Add new task\n"
         << "\tA - Show all tasks\n"
         << "\tT - Filter tasks by type\n"
         << "\tE - Edit a task\n"
         << "\tF - Finish a task\n"
         << "\tD - Delete a task\n"
         << "\tQ - Quit\n\n";
}


/**
 *  Writes the whole datastructure to file
 *
 *  @see   virtual Task::writeToFile(...)
 */
void writeToFile() {
    ofstream outfile("TODO.DTA");
    cout << "Writing to file ..." << '\n';

    if(outfile) {
        for (const auto & val : gTasks)
        {
            outfile << '\n';
            val->writeToFile(outfile);
        }
        outfile.close();
        cout << "Writing to file was successful." << '\n';
    }else {
        cout << "Error writing to file" << '\n';
    }

}

/**
 * Changes status of given task to finished
 *  @see void finishTask(...)
 */
void finishOneTask() {
    int taskNr;

    if(gTasks.empty()) {
        cout << "No tasks have been added, please add a task first!\n";
    }

    else {
        taskNr = lesInt("Task number", 1, gTasks.size());
        gTasks[taskNr-1]->finishTask();
    }
}

/**
 * Deletes a task
 */
void deleteTask() {
    int taskNr;

    if(gTasks.empty()) {
        cout << "No tasks have been added, please add a task first!\n";
    }

    else {
        cout << "Which task do you want to delete?\n";

        for(int i = 0; i < gTasks.size(); i++) {
            cout << "\nTask:\t" << i+1 << "\t(" << gTasks[i]->description << ')';
        }

        taskNr = lesInt("\n\nDelete task number", 1, gTasks.size());

        gTasks.erase(gTasks.begin() + taskNr-1);

        cout << "Task has been deleted!\n";
    }
}

/**
 * Sorts task by date or priority
 */
void writeAllTasks() {
    char sortChoice = ' ';
    string dateString;


    if (!gTasks.empty()) {
            while ((sortChoice != 'D') && (sortChoice != 'P') && (sortChoice != 'N')) {
            sortChoice = lesChar("\nSort by [D]ate, [P]riority or task[N]umber?");
        }

        if (sortChoice != 'N'){
            for (int i = 0; i < gTasks.size(); i++)
            {
                dateString = gTasks[i]->dateString();
                dateString += 'T';
                dateString += to_string(i);
                gSortedTasks.insert(pair <string,Task*>(dateString,gTasks[i]));
            }
        }

        switch(sortChoice) {
            case 'D': sortByDate();         break;
            case 'P': sortByPriority();     break;
            case 'N': sortByNumber();
        }
    }

}

void sortByDate() {

    for (const auto & val : gSortedTasks) {
        //cout << "\n" << val.first << endl;
        (val.second)->writeData();
        cout << "\n";
    }
}


void sortByPriority() {

    cout << "\nLow priority:\n\n";
    for (const auto & val : gSortedTasks) {
        if ((val.second)->priorityMatch('L')){
            (val.second)->writeData();
            cout << "\n";
        }
    }

    cout << "\nMedium priority:\n\n";
    for (const auto & val : gSortedTasks) {
        if ((val.second)->priorityMatch('M')){
            (val.second)->writeData();
            cout << "\n";
        }
    }

    cout << "\nHigh priority:\n\n";
    for (const auto & val : gSortedTasks) {
        if ((val.second)->priorityMatch('H')){
            (val.second)->writeData();
            cout << "\n";
        }
    }



    //if ((val.second)->typeMatch(...))
}
