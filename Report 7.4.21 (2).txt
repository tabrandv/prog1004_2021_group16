Report:

Date: 7. april 2021
Present at the meeting: Tom Arne Haugen Brandvold, Anders Lirhus, Sang Ngoc Nguyen, Mette Derås Waatsveen

Agenda: Plan and delegate further work. 

Sang and Tom Arne are going to work further with the wireframe and prototype. 
Anders will work with sequence diagram and use case diagram. 
Mette will work with Gantt chart, domain model and do user tests. 